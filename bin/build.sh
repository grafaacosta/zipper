#!/usr/bin/env nix-shell
#! nix-shell -i bash --pure
#! nix-shell -p jdk21 clojure git
#! nix-shell -I nixpkgs=https://github.com/NixOS/nixpkgs/archive/nixos-24.05.tar.gz

clojure -T:build build
