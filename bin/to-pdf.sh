#!/usr/bin/env bash

# Define the directory containing the .doc or .docx files
input_directory=$1

mkdir "$input_directory/pdf" > /dev/null 2>&1

# move pdf files directly into the folder
mv "$input_directory"/*.pdf "$input_directory/pdf/" > /dev/null 2>&1

# Loop through each .doc or .docx file in the directory
for file in "$input_directory"/*.doc "$input_directory"/*.docx
do
  # Check if the file exists (in case there are no .doc or .docx files)
  if [ -f "$file" ]; then
    # Convert the file to PDF using LibreOffice
    libreoffice --headless --convert-to pdf --outdir "$input_directory/pdf" "$file" && rm "$file"
  fi
done
