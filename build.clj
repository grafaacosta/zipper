(ns build
  (:require [clojure.tools.build.api :as b]
            [deps-deploy.deps-deploy :as dd]))

(def lib 'com.asmel/zipper)

(defn- clean [target]
  (b/delete {:path target})
  (println (format "Build folder %s removed" target)))

(defn- uber [{:keys [src-dirs version basis app-name main]}]
  (let [target  "target"
        class-dir (format "%s/classes" target)
        uber-file (format "%s-%s.jar" app-name version)
        basis (b/create-basis basis)]
    (clean target)

    (b/write-pom {:class-dir class-dir
                  :lib lib
                  :version version
                  :basis basis
                  :src-dirs ["src"]})

    (b/compile-clj {:basis basis
                    :src-dirs src-dirs
                    :class-dir class-dir})

    (b/uber {:class-dir class-dir
             :uber-file (str target "/" uber-file)
             :basis     basis
             :main      main})

    (println (format "Uber file created: \"%s\"" uber-file))))

(def build-config {:update-server {:version (format "1.0.%s" (b/git-count-revs nil))
                                   :src-dirs ["src"]
                                   :basis {:project "deps.edn"}
                                   :app-name (name lib)
                                   :main 'zipper.core}})

(defn build [& _]
  (-> build-config :update-server uber))

(defn deploy [_]
  (let [{:keys [app-name version]} (build-config :update-server)
        target "target"
        uber-file (format "%s-%s.jar" app-name version)]
    (dd/deploy {:installer :remote
                :artifact (str target "/" uber-file)

                :pom-file (b/pom-path {:lib lib
                                       :class-dir (format "%s/classes" target)})
                :repository "gitlab-maven"})))
