# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    grub = {
      enable = true;
      device = "/dev/sda";
    };
  };

  networking.hostName = "updator"; # Define your hostname.

  # Set your time zone.
  time.timeZone = "America/Buenos_Aires";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.gonza = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    packages = with pkgs; [ tree ];
  };

  users.users.javi = {
    isNormalUser = true;
    packages = with pkgs; [ tree ];
  };

  # List packages installed in system profile. To search, run:
  environment.systemPackages = with pkgs; [
    neovim
    htop
    wget
    git
    tmux
    magic-wormhole
    jdk21
    libreoffice-fresh
  ];

  programs.tmux = {
    enable = true;
    extraConfig = ''
      set -g set-clipboard external
      set -s copy-command 'xsel -i'

      #Prefix is Ctrl-a
      set -g prefix C-a
      bind C-a send-prefix
      unbind C-b

      set -sg escape-time 1
      set -g base-index 1
      setw -g pane-base-index 1

      #Mouse works as expected
      set -g mouse on

      setw -g monitor-activity on
      set -g visual-activity on

      set -g mode-keys vi
      set -g history-limit 10000

      # y and p as in vim
      bind Escape copy-mode
      unbind p

      bind p paste-buffer

      bind-key -T copy-mode-vi 'v' send -X begin-selection
      bind-key -T copy-mode-vi 'y' send -X copy-selection
      bind-key -T copy-mode-vi 'Space' send -X halfpage-down
      bind-key -T copy-mode-vi 'Bspace' send -X halfpage-up

      # extra commands for interacting with the ICCCM clipboard
      bind C-c run "tmux save-buffer - | xclip -i -sel clipboard"
      bind C-v run "tmux set-buffer \"$(xclip -o -sel clipboard)\"; tmux paste-buffer"

      # easy-to-remember split pane commands
      bind | split-window -h -c '#{pane_current_path}'
      bind - split-window -v -c '#{pane_current_path}'
      unbind '"'
      unbind %

      # moving between panes with vim movement keys
      bind h select-pane -L
      bind j select-pane -D
      bind k select-pane -U
      bind l select-pane -R

      # moving between windows with vim movement keys
      bind -r C-h select-window -t :-
      bind -r C-l select-window -t :+

      # resize panes with vim movement keys
      bind -r H resize-pane -L 5
      bind -r J resize-pane -D 5
      bind -r K resize-pane -U 5
      bind -r L resize-pane -R 5
    '';
  };

  services.samba = {
    enable = true;
    securityType = "user";
    openFirewall = true;
    extraConfig = ''
      workgroup = WORKGROUP
      server string = updator
      netbios name = updator
      security = user
      #use sendfile = yes
      #max protocol = smb2
      # note: localhost is the ipv6 localhost ::1
      hosts allow = 10.0.10. 192.168.0. 127.0.0.1 localhost
      hosts deny = 0.0.0.0/0
      guest account = nobody
      map to guest = bad user
    '';
    shares = {
      info = {
        path = "/home/gonza/info";
        browseable = "yes";
        "read only" = "no";
        "guest ok" = "no";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "gonza";
        "force group" = "users";
      };

      tmp = {
        path = "/home/gonza/zipper/tmp";
        browseable = "yes";
        "read only" = "yes";
        "guest ok" = "no";
        "create mask" = "0644";
        "directory mask" = "0755";
        "force user" = "gonza";
        "force group" = "users";
      };
    };
  };

  services.samba-wsdd = {
    enable = true;
    openFirewall = true;
  };

  services.openssh = {
    enable = true;
    settings = {
       PasswordAuthentication = true;
       PermitRootLogin = "no";
    };
  };

  systemd.services.zipper = {
    enable = true;
    description = "Upload reports to godaddy";
    wantedBy = [ "multi-user.target" ];

    # Specify the service using systemd
    serviceConfig = {
      User = "gonza";
      Environment = ''"PATH=/run/current-system/sw/bin"'';
      ExecStart = "${pkgs.jdk}/bin/java -jar /home/gonza/zipper/zipper.jar";
      Restart = "on-failure";
      WorkingDirectory =
        "/home/gonza/zipper/"; # Optional: specify the working directory
    };
  };

  services.tailscale.enable = true;

  # Open ports in the firewall.
  networking.firewall.enable = true;
  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [ 22 ];
  system.stateVersion = "24.05"; # Did you read the comment?

}

