(ns zipper.core
  (:require
   [clojure.core.async :as async]
   [clojure.java.io :as io]
   [clojure.string :as str]
   [clojure.java.shell :as sh]
   [outpace.config :as config]
   [taoensso.timbre :as timbre]
   [taoensso.timbre.appenders.3rd-party.rolling :as rolling])
  (:import
   [java.io File ByteArrayInputStream ByteArrayOutputStream]
   [java.security Security]
   [java.util.zip ZipEntry ZipOutputStream]
   [org.jodconverter.local JodConverter]
   [org.jodconverter.local.office LocalOfficeManager]
   [org.jodconverter.core.office OfficeUtils]
   [org.jodconverter.core.document DefaultDocumentFormatRegistry]
   [org.bouncycastle.jce.provider BouncyCastleProvider]
   [net.schmizz.sshj SSHClient]
   [net.schmizz.sshj.transport.verification PromiscuousVerifier]
   [net.schmizz.sshj.xfer FileSystemFile]
   [net.schmizz.sshj.xfer.scp SCPFileTransfer]
   [net.schmizz.sshj.userauth.keyprovider KeyProvider])
  (:gen-class))

(declare DELETE-FILES)
(declare SOURCE-PATH)
(declare MAX-COUNT)
(declare TMP-FOLDER)
(declare FILE-FILTER)
(declare HOST)
(declare USER)
(declare PORT)
(declare PASSPHRASE)
(declare DEST-PATH)
(declare DEST-PATH)
(declare INTERVAL)
(declare KEY-PATH)

(config/defconfig  DELETE-FILES true)
(config/defconfig  SOURCE-PATH nil)
(config/defconfig  MAX-COUNT 1000)
(config/defconfig  INTERVAL (* 6 3600 1000))
(config/defconfig  TMP-FOLDER
  (System/getProperty "java.io.tmpdir"))
(config/defconfig  FILE-FILTER "(.*?)")
(config/defconfig! HOST)
(config/defconfig! USER)
(config/defconfig  PASSPHRASE)
(config/defconfig  PORT 22)
(config/defconfig! DEST-PATH)

(def default-rsa (->> [(System/getProperty "user.home") ".ssh" "id_rsa"] (str/join File/separator)))
(config/defconfig  KEY-PATH default-rsa)

(defn get-ini-files [src]
  (->> src
       io/file
       file-seq
       (map (fn [^File file]
              {:size     (.length file)
               :filename (.getName file)
               :file file}))
       (filter (comp (partial re-find (re-pattern FILE-FILTER))
                     :filename))))

(defmacro ^:private with-entry
  [zip entry-name & body]
  `(let [^ZipOutputStream zip# ~zip]
     (.putNextEntry zip# (ZipEntry. ^java.lang.String ~entry-name))
     ~@body
     (flush)
     (.closeEntry zip#)))

(defmulti ->zip (fn [{name :filename} _] (cond
                                           (re-find #"\.docx?$" name) ::doc
                                           (re-find #"\.pdf$"   name) ::pdf
                                           :else ::doc)))

(defmethod ->zip ::doc [{name :filename file :file :as entry} zip]
  (let [entry-name (str/replace name #"docx?" "pdf")
        xout (ByteArrayOutputStream.)]
    (try (let [conversion (JodConverter/convert (io/file file))
               t (.to conversion xout true)
               in-format (.as t DefaultDocumentFormatRegistry/PDF)]
           (.execute in-format)
           (with-entry zip entry-name
             (io/copy (ByteArrayInputStream. (.toByteArray xout)) zip)))
         entry
         (catch Exception e (timbre/error (format "There was an error adding a file to the zip :: %s" (ex-message e)))
                (.close xout)
                nil))))

(defmethod ->zip ::pdf [{name :filename file :file :as entry} zip]
  (try (with-open [xin (io/input-stream file)]
         (with-entry zip name
           (io/copy xin zip)))
       entry
       (catch Exception e (timbre/error (format "There was an error adding a file to the zip :: %s" (ex-message e))) nil)))

(defn- make-zip [files id]
  (let [path (str TMP-FOLDER "/" id ".zip")
        zip (ZipOutputStream. (io/output-stream path))
        processed (doall (->> files
                              (map #(->zip % zip))
                              (filter identity)))]
    (.close zip)
    {:files processed :id id :path path}))

(defn gen-uuids []
  (iterate (fn [_] (java.util.UUID/randomUUID)) (java.util.UUID/randomUUID)))

(defn- add-auth-key [^SSHClient client key-path]
  (when (.exists (File. ^String key-path))
    [(.loadKeys client ^java.lang.String key-path (char-array PASSPHRASE))]))

(defn check-bouncy-castle! [retry]
  (Security/addProvider (BouncyCastleProvider.))
  (if (Security/getProvider "BC")
    (timbre/info "BouncyCastle provider added.")
    (if (< retry 1)
      (recur (inc retry))
      (timbre/error "Failed to add BouncyCastle provider."))))

(defn- create-ssh-client []
  (check-bouncy-castle! 0)
  (let [client (doto (SSHClient.)
                 (.addHostKeyVerifier (PromiscuousVerifier.))
                 (.loadKnownHosts)
                 (.connect ^java.lang.String HOST ^int PORT))]
    (doto client
      #_(.authPublickey ^java.lang.String USER (into-array KeyProvider (add-auth-key client KEY-PATH)))
      (.authPassword ^java.lang.String USER (char-array PASSPHRASE)))))

(def client (atom nil))

(defn get-ssh-client []
  (loop []
    (if (and @client (.isConnected ^SSHClient @client))
      (do
        (timbre/info "ssh is connected!")
        @client)
      (do
        (async/<!! (async/timeout 5000))
        (timbre/info "reconnecting ssh!")
        (reset! client (create-ssh-client))
        (recur)))))

(defn- send-file [ssh zip]
  (try
    (let [^SCPFileTransfer file-transfer (.newSCPFileTransfer ^SSHClient ssh)]
      (.upload file-transfer
               (FileSystemFile. ^String (:path zip)) ^String DEST-PATH))
    (timbre/info (format "%s was uploaded with size %.4fM. Deleting source files." (:id zip) (-> (io/file (:path zip))
                                                                                                 (.length)
                                                                                                 (/ (* 1024 1024))
                                                                                                 double)))
    true
    (catch Exception e
      (timbre/error (format "There was an error uploading the file %s :: %s" (:id zip) (ex-message e)))
      false)))

(defn delete-files [files]
  (when DELETE-FILES
    (doseq [{file :file filename :filename} files]
      (try (io/delete-file file)
           (catch Exception e (timbre/error (format "There was an error deleting the file %s :: %s" filename (ex-message e))))))))

(defn execute [zips]
  (for [zip zips]
    (when (send-file (get-ssh-client) zip)
      (delete-files (:files zip)))))

(defn setup-timbre! [logs]
  (timbre/merge-config!
   {:appenders {:rolling-appender
                (rolling/rolling-appender
                 {:path logs
                  :pattern :daily})}
    :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}})
  (timbre/merge-config!
   {:output-opts {:stacktrace-fonts {}}
    :appenders {:println          {:enabled? true}
                :syslog-appender  {:enabled? false}
                :rolling-appender {:enabled? true}}}))

(defn -main []
  (setup-timbre! (format "%szipper.log" TMP-FOLDER))

  (Thread/setDefaultUncaughtExceptionHandler
   (reify Thread$UncaughtExceptionHandler
     (uncaughtException [_ thread ex]
       (timbre/error ex "Uncaught exception on" (.getName thread)))))
  (loop []
    (sh/sh "./bin/to-pdf.sh" SOURCE-PATH) ;; while JOD converter does not work
    (when-let [chopped (->> (get-ini-files (str SOURCE-PATH "/pdf"))
                            (partition-all MAX-COUNT)
                            seq)]
      (->> (gen-uuids)
           (map make-zip chopped)
           execute
           doall))
    (Thread/sleep INTERVAL)
    (recur)))

(comment

  (def chopped (->> (get-ini-files (str SOURCE-PATH "/pdf"))
                    (partition-all MAX-COUNT)
                    seq))

  (def office-manager (-> (LocalOfficeManager/builder)
                          #_(.pipeNames (into-array String ["libreoffice-pipe"]))
                          (.officeHome "/nix/store/p4mal2xanaa045k0w81lrs12ysz1j30f-libreoffice-7.5.7.1-wrapped/lib/libreoffice" #_"/nix/store/2swa7l5ln47r7s4bb932h22nzb1zqgra-libreoffice-24.2.5.2/lib/libreoffice/" #_"/nix/store/0hz68ih2w7ni9g8cx46hdw133gq8kz6k-libreoffice-24.2.5.2-wrapped/lib/libreoffice")
                          (.install)
                          (.build)))

  (.start office-manager)

  (OfficeUtils/stopQuietly office-manager)

  (-> (JodConverter/convert (io/file "/home/gonza/Workspace/asmel/zipper/info/SCHMIDT_LUCAS2021111700585701.doc"))
      (.to (io/file "/home/gonza/Workspace/asmel/zipper/info/test.pdf"))
      (.execute))

  (def f (first chopped))

  (count f)

  (make-zip f (java.util.UUID/randomUUID))

  (->> (gen-uuids)
       (map make-zip chopped)
       execute
       doall)

  (-main)

;;
  )
